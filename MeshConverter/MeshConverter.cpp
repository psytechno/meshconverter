// MeshConverter.cpp : This file contains the 'main' function. Program execution begins and ends there.
//



#include <filesystem>
#include <map>

#include "MeshConverter.h"

using namespace std;

bool fileExists(const std::string& file) {
	struct stat info;

	if (stat(file.c_str(), &info) != 0)
		return false;
	else if (info.st_mode & S_IFREG)
		return true;
	else
		return false;
}

bool dirExists(const std::string& path)
{
	struct stat info;

	if (stat(path.c_str(), &info) != 0)
		return false;
	else if (info.st_mode & S_IFDIR)
		return true;
	else
		return false;
}

string getCurrentWorkingDirectory() {
	const int BUFSIZE = 4096;
	char buf[BUFSIZE];
	memset(buf, 0, BUFSIZE);
	GETCWD(buf, BUFSIZE - 1);
	return buf;
}

bool isTest(int argc, char* argv[]) {
	for (int i = 0; i < argc; i++) {
		if (!string("--test").compare(argv[i])) {
			return true;
		}
	}
	return false;
}
bool isAscii(int argc, char* argv[]) {
	for (int i = 0; i < argc; i++) {
		if (!string("--ascii").compare(argv[i])) {
			return true;
		}
	}
	return false;
}
string getPath(int argc, char* argv[]) {
	for (int i = 0; i < argc; i++) {
		string dir = string(argv[i]);		
		if (dirExists(dir)) {
			return dir;
		}
	}
	return getCurrentWorkingDirectory();
}

string getFile(int argc, char* argv[]) {
	if (argc > 1) {
		for (int i = 1; i < argc; i++) {
			string f = string(argv[i]);
			if (fileExists(f)) {
				return f;
			}
		}
	}
	return string();
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file


int MeshConverter::geoUnitsTotalSize()
{
	return elements.at(1)->size() + elements.at(2)->size() + elements.at(3)->size();
}

MeshConverter::MeshConverter()
{
	elements.insert_or_assign(1, new vector<Element*>());
	elements.insert_or_assign(2, new vector<Element*>());
	elements.insert_or_assign(3, new vector<Element*>());
}

MeshConverter::MeshConverter(string path) : MeshConverter()
{	
	setPath(path);
}


MeshConverter::~MeshConverter()
{
	closeInputs();
	if (inpXynTxt) delete inpXynTxt;
	if (inpBoundMif) delete inpBoundMif;
	if (inpNopTxt) delete inpNopTxt;
	if (inpGmsh) delete inpGmsh;
	if (outXynTxt) delete outXynTxt;
	if (outBoundMif) delete outBoundMif;
	if (outNopTxt) delete outNopTxt;
	if (outGmsh) delete outGmsh;

	for (map<int, vector<Element*>*>::iterator it = elements.begin(); it != elements.end(); ++it) {
		for (int i = 0; i < it->second->size(); i++) {
			delete it->second->at(i);			
		}
		delete it->second;
	}
	for (int i = 0; i < nodes.size(); i++) {
		delete nodes[i];
	}
}

bool MeshConverter::setPath(std::string path)
{		
	inpXynTxt = new ifstream(path + PATH_SEP + "XYN.TXT");
	inpBoundMif = new ifstream(path + PATH_SEP + "BOUND.MIF");
	inpNopTxt = new ifstream(path + PATH_SEP + "NOP.TXT");
	if (!inpXynTxt->is_open() || !inpBoundMif->is_open() || !inpNopTxt->is_open()) {
		if (inpXynTxt->is_open()) inpXynTxt->close();
		if (inpBoundMif->is_open()) inpBoundMif->close();
		if (inpNopTxt->is_open()) inpNopTxt->close();
		return false;
	}
	this->inpPath = path;
	this->outPath = path;
	cout << "Using " << path << endl;
	return true;
}

bool MeshConverter::ConvertToGmesh()
{
	if (!inputsOpen())
		return false;

	outGmsh = new ofstream(outPath + PATH_SEP + "out.gmsh");

	if (outGmsh && outGmsh->is_open()) {
		(*outGmsh) << "$MeshFormat" << endl;
		(*outGmsh) << "2.2 0 8" << endl;
		(*outGmsh) << "$EndMeshFormat" << endl;

		(*outGmsh) << "$Nodes" << endl;
		
		readXynTxt();

		(*outGmsh) << nodes.size() << endl;
		for (long i = 0; i < nodes.size(); i++) {
			(*outGmsh) << i + 1 << ' ' << nodes[i]->x << ' ' << nodes[i]->y << ' ' << nodes[i]->z << endl;
		}		
		(*outGmsh) << "$EndNodes" << endl;
		

		(*outGmsh) << "$Elements" << endl;
	
		readBoundMif();
		readNopTxt();



		(*outGmsh) << geoUnitsTotalSize() << endl;

		int counter = 1;
		for (map<int, vector<Element*>*>::iterator it = elements.begin(); it != elements.end(); ++it) {
			std::cout << it->first << " => " << it->second << '\n';
			for (long i = 0; i < it->second->size(); i++) {
				auto gu = it->second->at(i);
				if (gu) {
					(*outGmsh) << counter++ << " " << gu->nodes.size() - 1 << " 2 " << gu->parValues[0] << " " << gu->parValues[1];
					for (int j = 0; j < gu->nodes.size(); j++)
						(*outGmsh) << " " << gu->nodes[j];
					(*outGmsh) << endl;

				}
			}
		}
			

		

		(*outGmsh) << "$EndElements" << endl;

		outGmsh->close();

		return true;
	}
	return false;
}

bool MeshConverter::ConvertToGmeshBinary()
{
	if (!inputsOpen())
		return false;

	ofstream GMESH(outPath + PATH_SEP + "out.gmsh", ofstream::binary);	

	if (GMESH.is_open()) {
		GMESH << "$MeshFormat" << endl;
		
		GMESH << "2.2 1 8" << endl;
		int one = 1;
		GMESH.write(reinterpret_cast<char*> (&one), sizeof(int)); //'ONE-BINARY'
		GMESH << endl;

		GMESH << "$EndMeshFormat" << endl;

		GMESH << "$Nodes" << endl;

		readXynTxt();

		GMESH << nodes.size() << endl; //'NUMBER-OF-NODES'
		for (long i = 1; i < nodes.size() + 1; i++) {			
			GMESH.write(reinterpret_cast<char*> (&i), sizeof(int));//'NODES-BINARY' (index)
			double xyz[3] = { nodes[i - 1]->x, nodes[i - 1]->y, nodes[i - 1]->z };
			GMESH.write(reinterpret_cast<char*>(xyz), 3 * sizeof(double)); //'NODES-BINARY' (xyz)
			if (i == nodes.size()) {
				GMESH << endl;
			}
		
		}
		GMESH << "$EndNodes" << endl;

		GMESH << "$Elements" << endl;

		readBoundMif();
		int numberOfSegments = geoUnitsTotalSize();
		readNopTxt();
		
		GMESH << geoUnitsTotalSize() << endl;

		int header[3] = { 0, 0, 2 };
		int unit[7];
		int counter = 1;
		for (map<int, vector<Element*>*>::iterator it = elements.begin(); it != elements.end(); ++it) {			
			header[0] = it->first;
			header[1] = it->second->size();
			GMESH.write(reinterpret_cast<char*>(header), 3 * sizeof(int)); //'ELEMENT-HEADER-BINARY'
			for (int i = 0; i < it->second->size(); i++) {
				auto gu = *it->second->at(i);
				unit[0] = counter++;
				unit[1] = gu.parValues[0];
				unit[2] = gu.parValues[1];
				if (gu.nodes.size() >= 1) unit[3] = gu.nodes[0];
				if (gu.nodes.size() >= 2) unit[4] = gu.nodes[1];
				if (gu.nodes.size() >= 3) unit[5] = gu.nodes[2];
				if (gu.nodes.size() >= 4) unit[6] = gu.nodes[3];
				GMESH.write(reinterpret_cast<char*>(unit), (gu.nodes.size() + 3) * sizeof(int));
			}
		}
		
		GMESH << endl;
		GMESH << "$EndElements" << endl;

		GMESH.close();
		return true;
	}
	return false;
}
Node *MeshConverter::getNodeFromXynTxnLine(std::string line)
{
	std::string::size_type sz;     // alias of size_t

	double v[3] = { 0, 0, 0 };
	
	int i = 0;
	try {
		while (line.size() > 0 && i < 3) {
			v[i] = stod(line, &sz);
			line = line.substr(sz);
			i++;
		}
	}
	catch (exception ex) {

	}
	return new Node(v);
}

Node* MeshConverter::getNodeFromGmeshLine(string line)
{
	std::string::size_type sz;     // alias of size_t

	double v[3] = { 0, 0, 0 };
	
	int index = stoi(line, &sz);
	line = line.substr(sz);

	int i = 0;
	try {
		while (line.size() > 0 && i < 3) {
			v[i] = stod(line, &sz);
			line = line.substr(sz);
			i++;
		}
	}
	catch (exception ex) {

	}
	return new Node(v, index);
}

Element *MeshConverter::getSegment(string line1, string line2)
{
	std::string::size_type sz;     // alias of size_t

	int n1 = stoi(line1, &sz);
	int index = stoi(line1.substr(sz));
	int n2 = stoi(line2);
	return new Element(n1, n2, 0, 0, 0, index);
	//return GeoUnit(n1, n2, 0, 0, 0x7fffffff, 0x7fffffff);
}

Element *MeshConverter::getGeoUnit(string line)
{
	std::string::size_type sz;     // alias of size_t
	
	int n1 = 0, n2 = 0, n3 = 0, n4 = 0;

	n1 = stoi(line, &sz);
	line = line.substr(sz);
	n2 = stoi(line, &sz);
	line = line.substr(sz);
	if (line.length() > 0) {
		n3 = stoi(line, &sz);
		line = line.substr(sz);
		if (line.length() > 0) {
			n4 = stoi(line, &sz);
		}
	}

	return new Element(n1, n2, n3, n4, 0, 0);
	//return GeoUnit(n1, n2, n3, n4, 0x7fffffff, 0x7fffffff);
	
}
Element* MeshConverter::getGeoUnitFromGmeshLine(string line)
{
	std::string::size_type sz;
	int index = stoi(line, &sz);
	line = line.substr(sz);
	int elmType = stoi(line, &sz);
	line = line.substr(sz);
	int numTags = stoi(line, &sz);
	line = line.substr(sz);
	int t = 0, i = 0;
	auto el = new Element(index);
	while (line.size() > 0 && t < numTags) {
		el->parValues.push_back(stoi(line, &sz));
		line = line.substr(sz);
		t++;
	}
	while (line.size() > 0 && i <= elmType) {
		el->nodes.push_back(stoi(line, &sz));
		line = line.substr(sz);
		i++;
	}
	return el;
}
bool MeshConverter::inputsOpen()
{
	return inpXynTxt && inpXynTxt->is_open() && inpBoundMif&& inpBoundMif->is_open() && inpNopTxt&& inpNopTxt->is_open();
}
void MeshConverter::closeInputs() {
	if (inpBoundMif && inpBoundMif->is_open()) inpBoundMif->close();
	if (inpNopTxt && inpNopTxt->is_open()) inpNopTxt->close();
	if (inpXynTxt && inpXynTxt->is_open()) inpXynTxt->close();
}
void MeshConverter::readXynTxt()
{
	string line;
	while (getline(*((ifstream *)inpXynTxt), line))
	{
		nodes.push_back(getNodeFromXynTxnLine(line));
	}
}
void MeshConverter::readBoundMif()
{
	string line, prevLine="";
	auto lines = elements.at(1);
	while (getline(*inpBoundMif, line))
	{
		if (line.c_str()[0] == 'P') {
			lines->reserve(lines->size() + stoi(line.substr(6)));
		}
		else if (isdigit(line.c_str()[0]) && isdigit(prevLine.c_str()[0])) {
			lines->push_back(getSegment(line, prevLine));
		}
		prevLine = line;
	}
}
void MeshConverter::readNopTxt()
{
	string line;
	while (getline(*inpNopTxt, line))
	{
		auto gu = getGeoUnit(line);
		if (gu) {
			if (gu->nodes.size() == 2)
				elements.at(1)->push_back(gu);
			if (gu->nodes.size() == 3)
				elements.at(2)->push_back(gu);
			if (gu->nodes.size() == 4)
				elements.at(3)->push_back(gu);
		}
	}
}

void MeshConverter::readGmesh(string fileName, string * _outPath)
{
	inpGmsh = new ifstream(fileName);

	auto throwException = [](string msg, ifstream *f){ throw exception((msg + ((f == nullptr) ? (", at position " + std::to_string(f->tellg())) : "")).c_str()); };
	
	if (!inpGmsh || !inpGmsh->is_open()) {
		throwException("File cannot be opened for reading - " + fileName, inpGmsh);
	}

	string line;
	int nElements;
	int nNodes;

	auto readEndMeshFormatAndNodesHeader = [&]() { 
		getline(*inpGmsh, line);
		if (string("$EndMeshFormat").compare(line)) {
			throwException("'$EndMeshFormat' expected", inpGmsh);
		}
		getline(*inpGmsh, line);
		if (string("$Nodes").compare(line)) {
			throwException("'$Nodes' expected", inpGmsh);
		}
		getline(*inpGmsh, line);

		try { nodes.reserve(stoi(line)); }
		catch (std::bad_alloc ex) { throwException("Out of memory (" + line + ")", inpGmsh); }
		catch (exception ex) { throwException("Not a valid integer", inpGmsh); }

		nNodes = nodes.capacity();
	};

	auto readEndNodesAndElementsHeader = [&]() {
		getline(*inpGmsh, line);
		if (string("$EndNodes").compare(line)) {
			throwException("'$EndNodes' expected", inpGmsh);
		}
		getline(*inpGmsh, line);
		if (string("$Elements").compare(line)) {
			throwException("'$Elements' expected", inpGmsh);
		}
		getline(*inpGmsh, line);
		
		try { nElements = stoi(line); }
		catch (exception ex) { throwException("Not a valid integer", inpGmsh); }
		cout << "Number of elements = " << nElements << endl;
	};
	
	getline(*inpGmsh, line);

	if (string("$MeshFormat").compare(line)) {
		throwException("First line must be '$MeshFormat'", inpGmsh);
	}	
	getline(*inpGmsh, line);

	if (!string("2.2 0 8").compare(line)) {
		cout << "ascii file detected" << endl;
		readEndMeshFormatAndNodesHeader();
		int nNodes = nodes.capacity();
		cout << "Number of nodes = " << nNodes << endl;
		for (int i = 0; i < nNodes; i++) {
			getline(*inpGmsh, line);
			nodes.push_back(getNodeFromGmeshLine(line));
		}
		readEndNodesAndElementsHeader();
		for (int i = 0; i < nElements; i++) {
			getline(*inpGmsh, line);
			auto el = getGeoUnitFromGmeshLine(line);
			if (el->nodes.size()>=2 && el->nodes.size() <= 4)
				elements.at(el->nodes.size() - 1)->push_back(el);
		}
	} else if (!string("2.2 1 8").compare(line)) {
		cout << "binary file detected" << endl;
		inpGmsh->close();
		delete inpGmsh;
		inpGmsh = new ifstream(fileName, ifstream::binary);
		getline(*inpGmsh, line);
		getline(*inpGmsh, line);
		getline(*inpGmsh, line);
		cout << "ONE=" << (int)line[0] << endl;
		
		readEndMeshFormatAndNodesHeader();
		
		cout << "Number of nodes = " << nNodes << endl;
		uint32_t n;
		double x, y, z;
		for (int i = 0; i < nNodes; i++) {			
			inpGmsh->read(reinterpret_cast<char*>(&n), sizeof(n));
			inpGmsh->read(reinterpret_cast<char*>(&x), sizeof(x));
			inpGmsh->read(reinterpret_cast<char*>(&y), sizeof(y));
			inpGmsh->read(reinterpret_cast<char*>(&z), sizeof(z));
			nodes.push_back(new Node(x, y, z, n));
		}

		getline(*inpGmsh, line);
		
		readEndNodesAndElementsHeader();
		
		int blockType=1, remaining=0, nTags=0;
		int tmp;
		for (int j = 0; j < nElements; j++) {
			if (remaining == 0) {
				inpGmsh->read(reinterpret_cast<char*>(&blockType), sizeof(blockType));
				if (blockType < 1) throwException("Invalid blockType=" + to_string(blockType), inpGmsh);
				if (blockType > 3) throwException("Unsupported blockType=" + to_string(blockType), inpGmsh);
				
				inpGmsh->read(reinterpret_cast<char*>(&remaining), sizeof(remaining));
				if (remaining < 1) throwException("Invalid block size=" + to_string(remaining), inpGmsh);

				inpGmsh->read(reinterpret_cast<char*>(&nTags), sizeof(nTags));
				if (nTags < 0) throwException("Invalid number of tags=" + to_string(nTags), inpGmsh);
			}
			Element * e = new Element();
			inpGmsh->read(reinterpret_cast<char*>(&tmp), sizeof(tmp));
			e->index = tmp;
			for (int k = 0; k < nTags; k++) {
				inpGmsh->read(reinterpret_cast<char*>(&tmp), sizeof(tmp));
				e->parValues.push_back(tmp);
			}
			for (int k = 0; k < blockType+1; k++) {
				inpGmsh->read(reinterpret_cast<char*>(&tmp), sizeof(tmp));
				e->nodes.push_back(tmp);
			}
			elements.at(blockType)->push_back(e);
			remaining--;
		}
			
	} else {
		throwException("Wrong file format (line 2)", inpGmsh);
	}

	cout << nodes.size() << " nodes" << endl;
	cout << elements.at(1)->size() << " elements of type 1" << endl;
	cout << elements.at(2)->size() << " elements of type 2" << endl;
	cout << elements.at(3)->size() << " elements of type 3" << endl;

	if (!_outPath) {
		filesystem::path p(fileName);
		p.remove_filename();
		outPath = p.string();
	}
	else {
		outPath = *_outPath;
	}
	//if
	

	//$EndMeshFormat		$Nodes

	
	cout << outPath;
}

void MeshConverter::writeXynTxt()
{
	outXynTxt = new ofstream(outPath + PATH_SEP + "XYN.TXT");

	if (!outXynTxt || !outXynTxt->is_open()) {
		throw exception((outPath + PATH_SEP + "XYN.TXT cannot be created").c_str());
	}

	for (int i = 0; i < nodes.size(); i++) {
		(*outXynTxt) << nodes[i]->x << "\t" << nodes[i]->y << endl;
	}

	outXynTxt->close();
}

void MeshConverter::writeNopTxt()
{
	if ((!elements.at(2) || elements.at(2)->size() == 0) && (!elements.at(3) || elements.at(3)->size() == 0)) {
		return;
	}

	outNopTxt = new ofstream(outPath + PATH_SEP + "NOP.TXT");

	if (!outNopTxt || !outNopTxt->is_open()) {
		throw exception((outPath + PATH_SEP + "NOP.TXT cannot be created").c_str());
	}

	for (map<int, vector<Element*>*>::iterator it = elements.begin(); it != elements.end(); ++it) {
		for (int i = 0; i < it->second->size(); i++) {
			if (it->first > 1 && it->second->at(i)) {
				int j = 0;
				while (j < it->second->at(i)->nodes.size()) {
					(*outNopTxt) << it->second->at(i)->nodes[j];
					j++;
					if (j < it->second->at(i)->nodes.size()) {
						(*outNopTxt) << ' ';
					}
				}
				while (j < 4) {
					(*outNopTxt) << " 0";
					j++;
				}
				(*outNopTxt) << endl;
			}
		}
		

	}

	outNopTxt->close();
}

void MeshConverter::writeBoundMif()
{
	if (!elements.at(1) || elements.at(1)->size() == 0) {
		return;
	}

	outBoundMif = new ofstream(outPath + PATH_SEP + "BOUND.MIF");
		
	if (!outBoundMif || !outBoundMif->is_open()) {
		throw exception((outPath + PATH_SEP + "BOUND.MIF cannot be created").c_str());
	}

	(*outBoundMif) << "Data" << endl;

	map<int, vector<Element *>> elementsByPline;

	int pLine = 0;
	for (int i = 0; i < elements.at(1)->size(); i++) {
		if (elements.at(1)->at(i) && elements.at(1)->at(i)->parValues.size() >= 2) {
			pLine = elements.at(1)->at(i)->parValues[1];
		}
		auto it = elementsByPline.find(pLine);
		if (it == elementsByPline.end()) {
			elementsByPline.insert_or_assign(pLine, vector<Element*>());
			it = elementsByPline.find(pLine);
		}
		
		it->second.push_back(elements.at(1)->at(i));
	}
	for (auto it = elementsByPline.begin(); it != elementsByPline.end(); ++it) {
		(*outBoundMif) << "Pline " << it->second.size() + 1 << endl;
		(*outBoundMif) << it->second.at(0)->nodes[1] << '\t' << it->first << endl;
		for (int i = 0; i < it->second.size(); i++) {
			(*outBoundMif) << it->second.at(i)->nodes[0] << '\t' << it->first << endl;
		}
		(*outBoundMif) << "*" << endl;
	}
	outBoundMif->close();
}
