#define CATCH_CONFIG_RUNNER
#include "catch.hpp"
#include <filesystem>
#include "MeshConverter.h"

int main(int argc, char* argv[])
{	
	cout << fileExists("c:\\Users\\Plazmer\\source\\repos\\maniwani\\") << endl;

	if (isTest(argc, argv)) {
		char** argv_new = new char* [argc - 1];
		for (int i = 0; i < argc - 1; i++) {
			argv_new[i] = argv[i + 1];
		}
		argv_new[0] = argv[0];
		int result = Catch::Session().run(/*argc - */1, argv_new);
	}

	MeshConverter mc;	



	string path = getFile(argc, argv);

	if (path.empty())
	{
		path = getPath(argc, argv);

		if (!mc.setPath(path)) {
			cout << "Path " << path << " does not contain necessary files (BOUND.MIF, NOP.TXT, XYN.TXT)" << endl;
			return -1;
		}

		if (mc.inputsOpen()) {
			bool binaryFormat = true;
			if (isAscii(argc, argv)) {
				binaryFormat = false;
				cout << "ASCII mode" << endl;
			}

			bool result = false;
			if (binaryFormat) {
				result = mc.ConvertToGmeshBinary();
			}
			else {
				result = mc.ConvertToGmesh();
			}
			if (result) {
				cout << "Conversion complete successfully, check output in out.gmsh";
			}
		}
	}
	else {
		cout << "Using GMESH file " << path << endl;
		string outPath = getPath(argc, argv);
		cout << "Using output path " << outPath << endl;
		try {
			mc.readGmesh(path, &outPath);
			mc.writeXynTxt();
			mc.writeBoundMif();
			mc.writeNopTxt();
		}
		catch (exception ex) {
			cout << ex.what() << endl;
		}
	}
	return 0;
}

TEST_CASE("getSegment ok", "[read]") {
	MeshConverter mc;
	auto seg = *mc.getSegment("100 1", "101 1");
	REQUIRE(seg.nodes.size() == 2);
	REQUIRE(seg.nodes[0] == 100);
	REQUIRE(seg.nodes[1] == 101);
	REQUIRE(seg.parValues[0] == 0);
	REQUIRE(seg.parValues[1] == 1);
	seg = *mc.getSegment("102 2", "1 2");
	REQUIRE(seg.nodes.size() == 2);
	REQUIRE(seg.nodes[0] == 102);
	REQUIRE(seg.nodes[1] == 1);
	REQUIRE(seg.parValues[0] == 0);
	REQUIRE(seg.parValues[1] == 2);
}
TEST_CASE("getSegment fail", "[read]") {
	MeshConverter mc;
	REQUIRE_THROWS_AS(mc.getSegment("101 1", "Pline 101"), std::invalid_argument);
	REQUIRE_THROWS_AS(mc.getSegment("Pline 101", "Data"), std::invalid_argument);
	REQUIRE_THROWS_AS(mc.getSegment("*", "1 1"), std::invalid_argument);
	REQUIRE_THROWS_AS(mc.getSegment("Pline 1001", "*"), std::invalid_argument);
}
TEST_CASE("getGeoUnit ok", "[read]") {
	MeshConverter mc;
	auto gu = *mc.getGeoUnit("1	102	103	2");
	REQUIRE(gu.nodes.size() == 4);
	REQUIRE(gu.nodes[0] == 1);
	REQUIRE(gu.nodes[1] == 102);
	REQUIRE(gu.nodes[2] == 103);
	REQUIRE(gu.nodes[3] == 2);
	REQUIRE(gu.parValues[0] == 0);
	REQUIRE(gu.parValues[1] == 0);

	gu = *mc.getGeoUnit("1	102	103");
	REQUIRE(gu.nodes.size() == 3);
	REQUIRE(gu.nodes[0] == 1);
	REQUIRE(gu.nodes[1] == 102);
	REQUIRE(gu.nodes[2] == 103);

	gu = *mc.getGeoUnit("1	102");
	REQUIRE(gu.nodes.size() == 2);
	REQUIRE(gu.nodes[0] == 1);
	REQUIRE(gu.nodes[1] == 102);

	gu = *mc.getGeoUnit("633    634    632      0");
	REQUIRE(gu.nodes.size() == 3);
	REQUIRE(gu.nodes[0] == 633);
	REQUIRE(gu.nodes[1] == 634);
	REQUIRE(gu.nodes[2] == 632);
}