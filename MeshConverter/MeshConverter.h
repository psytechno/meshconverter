#pragma once

#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <vector>

#ifdef _WIN32
#include "direct.h"
#define PATH_SEP '\\'
#define GETCWD _getcwd
#else
#include "unistd.h"
#define PATH_SEP '/'
#define GETCWD getcwd
#endif

using namespace std;

bool dirExists(const std::string& path);
bool fileExists(const std::string& file);
string getCurrentWorkingDirectory();
bool isTest(int argc, char* argv[]);
bool isAscii(int argc, char* argv[]);
string getPath(int argc, char* argv[]);
string getFile(int argc, char* argv[]);

struct Node
{
public:
	Node(double v[], int index = 0) {
		this->x = v[0];
		this->y = v[1];
		this->z = v[2];
		this->index = index;
	}
	Node(double x, double y, double z, int index=0) {
		this->x = x;
		this->y = y;
		this->z = z;
		this->index = index;
	}
	double x, y, z;
	int index;
};

struct Element {
public:
	Element(int index=0) {
		this->index = index;
	}
	Element(int n1, int n2, int n3, int n4, int p1, int p2, int index=0) : Element(index) {
		if (n1 > 0) nodes.push_back(n1);
		if (n2 > 0) nodes.push_back(n2);
		if (n3 > 0) nodes.push_back(n3);
		if (n4 > 0) nodes.push_back(n4);
		parValues.push_back(p1);
		parValues.push_back(p2);
	}
	int index;
	vector<int> nodes;
	vector<int> parValues;
};

class MeshConverter
{
	string inpPath = "";
	string outPath = "";
	ifstream * inpXynTxt=nullptr, * inpBoundMif=nullptr, * inpNopTxt=nullptr, * inpGmsh = nullptr;
	ofstream * outXynTxt=nullptr, * outBoundMif=nullptr, * outNopTxt=nullptr, * outGmsh = nullptr;
	map<int, vector<Element*>*> elements;
	vector<Node*> nodes;
	int geoUnitsTotalSize();
public:
	MeshConverter();
	MeshConverter(string path);
	~MeshConverter();
	bool setPath(string path);
	bool ConvertToGmesh();
	bool ConvertToGmeshBinary();
	Node * getNodeFromXynTxnLine(string line);
	Node * getNodeFromGmeshLine(string line);
	Element * getSegment(string line1, string line2);
	Element * getGeoUnit(string line);
	Element* getGeoUnitFromGmeshLine(string line);
	bool inputsOpen();
	void closeInputs();
	void readXynTxt();
	void readBoundMif();
	void readNopTxt();

	void readGmesh(string fileName, string *_outPath = nullptr);
	void writeXynTxt();
	void writeNopTxt();
	void writeBoundMif();
};

